/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {existsSync, mkdirSync, readdir, readdirSync, readFileSync, writeFileSync} from 'fs';
import {Schema, ValidationError, Validator, ValidatorResult} from 'jsonschema';
import {join} from 'path';
import {promisify} from 'util';
import {escapeHtml, isThingWithType, logger} from './helpers';

const readdirPromisified = promisify(readdir);

/**
 * StAppsCore validator
 */
export class SCValidator {
  private path: string;
  private schemas: any;
  private validator: Validator;

  /**
   * Create a new validator
   *
   * @param path {string} The path to the schema files
   */
  constructor(path: string) {
    if (path.length < 1) {
      logger.error('Path can not be empty!');
      process.exit(1);
    }

    if (path[path.length - 1] !== '/') {
      path += '/';
    }

    this.validator = new Validator();
    this.path = path;
    this.schemas = {};
  }

  /**
   * Feed the schema files to a validator
   *
   * @returns {string[]} A list of schema files that were fed to the validator
   */
  public feedValidator(): string[] {
    let schemaFiles: string[];

    try {
      schemaFiles = readdirSync(this.path);
    } catch (err) {
      logger.error('Could not read directory `' + this.path + '` because of: ' + err);
      process.exit(1);
      // typescript doesn't know that process.exit(1) will terminate the Node.js process
      return [];
    }

    if (schemaFiles.length === 0) {
      logger.error('No schema files in `' + this.path + '`.');
      process.exit(1);
    }

    logger.info('Feeding schema files to validator...');

    schemaFiles.forEach((file, idx) => {
      if (!file.match(/\.json$/)) {
        schemaFiles.splice(idx);
        return;
      }

      let fileBuffer: Buffer;
      let schema: Schema;

      try {
        fileBuffer = readFileSync(this.path + file);
        schema = JSON.parse(fileBuffer.toString());
      } catch (e) {
        logger.error('Could not parse `' + file + '` because of: ' + e.stack);
        process.exit(1);
        return;
      }

      try {
        this.validator.addSchema(schema);
      } catch (e) {
        logger.error('Could not add `' + file + '` because of: ' + e.stack);
        process.exit(1);
      }

      this.schemas[file] = schema;

      logger.info('Validator ate file `' + file + '`.');
    });

    logger.ok('Validator is full.');

    return schemaFiles;
  }

  /**
   * Validates anything against a given schema name
   * @param instance
   * @param schemaName
   */
  public validate(instance: any, schemaName: string): ValidatorResult {
    return this.validator.validate(instance, this.schemas[schemaName + '.json']);
  }

  /**
   * Validate an instance of a thing against the consumed schema files (kept for legacy purposes)
   *
   * @param instance The thing to validate
   */
  public validateThing<T>(instance: T): ValidatorResult {
    if (!isThingWithType(instance)) {
      throw new Error('Instance.type does not exist.');
    }

    const schemaName = instance.type.split(' ').map((part) => {
      return part.substr(0, 1).toUpperCase() + part.substr(1);
    }).join('');

    return this.validate(instance, schemaName);
  }
}

/**
 * Validate all test files in the given resources directory against schema files in the given (schema) directory
 *
 * @param schemaDir The directory where the JSON schema files are
 * @param resourcesDir The directory where the test files are
 * @param reportDir The directory where the validation report should be saved into
 * @returns {Promise<any[]>}
 */
export async function validateFiles(schemaDir: string, resourcesDir: string, reportDir?: string): Promise<any> {
  if (typeof schemaDir !== 'string') {
    throw Error('Invalid schema directory provided');
  }

  if (typeof resourcesDir !== 'string') {
    throw Error('Invalid resources directory provided');
  }

  if (typeof reportDir !== 'string') {
    reportDir = 'report';
  }
  const v = new SCValidator(join(schemaDir));
  v.feedValidator();

  const testFiles = await readdirPromisified(resourcesDir);

  if (testFiles.length === 0) {
    throw new Error('No test files in `' + resourcesDir + '`.');
  }

  const errors: { [key: string]: ValidationError[] } = {};
  const expectedErrors: { [key: string]: ValidationError[] } = {};
  let amountExpectedErrors = 0;

  testFiles.forEach((testFile) => {
    if (!testFile.match(/\.json$/)) {
      return;
    }

    const instance: any = JSON.parse(readFileSync(join(resourcesDir, testFile)).toString());
    const validatorResult: ValidatorResult = v.validate(instance.scData, testFile.substring(0, testFile.indexOf('.')));
    const expectedErrorNames: string[] = instance.validationErrorNames;
    amountExpectedErrors += expectedErrorNames.length;

    validatorResult.errors.forEach((error: ValidationError) => {
      if (expectedErrorNames.indexOf(error.name) > -1) {
        expectedErrors[testFile] = expectedErrors[testFile] === undefined ? [] : expectedErrors[testFile];
        expectedErrors[testFile].push(error);
        expectedErrorNames.splice(expectedErrorNames.indexOf(error.name), 1);
        return;
      }
      errors[testFile] = errors[testFile] === undefined ? [] : errors[testFile];
      logger.error('Unexpected ValidationError named: ' + error.name);
      errors[testFile].push(error);
    });

    if (validatorResult.errors.length - expectedErrorNames.length !== validatorResult.errors.length) {
      // tslint:disable-next-line:max-line-length
      logger.error('`' + testFile + '` caused ' + (validatorResult.errors.length - instance.validationErrorNames.length) + ' unexpected error(s)!');
    } else {
      logger.info('Validated `' + testFile + '` against schema `' + testFile);
    }
  });

  const totalErrors: number = Object.keys(errors).map((testFile) => {
    return errors[testFile].length;
  }).reduce((previousValue, currentValue) => {
    return previousValue + currentValue;
  }, 0);

  const totalExpectedErrors: number = Object.keys(expectedErrors).map((testFile) => {
    return expectedErrors[testFile].length;
  }).reduce((previousValue, currentValue) => {
    return previousValue + currentValue;
  }, 0);

  let
    html = '<!doctype html>' +
      '<html lang="en">' +
      '<head>' +
      '<title>Validation result</title>' +
      '<meta charset="utf-8">' +
      '<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">' +
      '<link rel="stylesheet"' +
      'href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" ' +
      'integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb"' +
      'crossorigin="anonymous">' +
      '</head>' +
      '<body>' +
      '<div class="container-fluid">' +
      '<h1>Validation result</h1>' +
      ((totalErrors === 0)
          ? '<p class="text-success">Validation OK, no unexpected errors.</p>'
          : '<p class="text-danger">Validation failed, ' + totalErrors + ' unexpected errors</p>'
      ) +
      ((totalExpectedErrors !== amountExpectedErrors)
          // tslint:disable-next-line:max-line-length
          ? '<p class="text-danger">True negative validation failed, ' + (totalExpectedErrors - amountExpectedErrors) + ' expected errors haven\'t been thrown</p>'
          : '<p class="text-success">True negative validation OK, all expected errors were thrown.</p>'
      ) +
      '<p>' + (new Date()) + '</p>';

  if (totalErrors > 0) {
    Object.keys(errors).forEach((testFile) => {
      html += '<h2>Errors in <code>' + testFile + '</code></h2>';
      html += '<table class="table table-striped table-hover table-responsive">' +
        '<thead>' +
        '<tr>' +
        '<th>#</th>' +
        '<th>Error</th>' +
        '<th>Schema</th>' +
        '</tr>' +
        '</thead>' +
        '<tbody>';

      errors[testFile].forEach((error: ValidationError, idx: number) => {
        html += '<tr>' +
          '<td><h3>' + (idx + 1) + '</h3></td>' +
          '<td style="width: 50%">' +
          '<div class="alert alert-danger">' + escapeHtml(error.message) + '</div>' +
          '<pre>' +
          error.property.replace(/^instance\./, '') + ' = ' +
          escapeHtml(JSON.stringify(error.instance, null, 2)) +
          '</pre></td>' +
          '<td><pre>' + escapeHtml(JSON.stringify(error.schema, null, 2)) + '</pre></td>' +
          '</tr>';
      });
      html += '</tbody>' +
        '</table>';
    });
  }

  if (totalExpectedErrors > 0) {
    Object.keys(expectedErrors).forEach((testFile) => {
      html += '<h2>Expected errors in <code>' + testFile + '</code></h2>';
      html += '<table class="table table-striped table-hover table-responsive">' +
        '<thead>' +
        '<tr>' +
        '<th>#</th>' +
        '<th>Error</th>' +
        '<th>Schema</th>' +
        '</tr>' +
        '</thead>' +
        '<tbody>';

      expectedErrors[testFile].forEach((error: ValidationError, idx: number) => {
        html += '<tr>' +
          '<td><h3>' + (idx + 1) + '</h3></td>' +
          '<td style="width: 50%">' +
          '<div class="alert alert-success">' + escapeHtml(error.message) + '</div>' +
          '<pre>' +
          error.property.replace(/^instance\./, '') + ' = ' +
          escapeHtml(JSON.stringify(error.instance, null, 2)) +
          '</pre></td>' +
          '<td><pre>' + escapeHtml(JSON.stringify(error.schema, null, 2)) + '</pre></td>' +
          '</tr>';
      });
      html += '</tbody>' +
        '</table>';
    });
  }

  /* tslint:disable:max-line-length */
  html += '</div>' +
    '<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" crossorigin="anonymous" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"></script>' +
    '<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" crossorigin="anonymous" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh"></script>' +
    '<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" crossorigin="anonymous" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ"></script>' +
    '</body>' +
    '</html>';
  /* tslint:enable */

  if (!existsSync(reportDir)) {
    mkdirSync(reportDir);
  }

  writeFileSync(join(reportDir, 'index.html'), html);

  if (totalErrors === 0) {
    logger.ok('Validation finished.');
  } else {
    logger.error('Validation finished with ' + totalErrors + ' unexpected errors.');
  }

  if (totalErrors > 0) {
    throw totalErrors;
  }

  return 0;
}
