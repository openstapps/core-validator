/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Logger} from '@openstapps/logger';

export const logger = new Logger();

/**
 * Escape HTML special chars in a string
 *
 * @param text {string} The string to escape the special chars in
 * @returns {string} Escaped string
 *
 * @see http://stackoverflow.com/questions/1787322/htmlspecialchars-equivalent-in-javascript
 */
export function escapeHtml(text: any): string {
  if (typeof text !== 'string') {
    text = JSON.stringify(text, null, 2);
  }

  if (typeof text === 'undefined') {
    text = 'undefined';
  }

  const map: { [s: string]: string } = {
    '"': '&quot;',
    '&': '&amp;',
    "'": '&#039;',
    '<': '&lt;',
    '>': '&gt;',
  };

  return text.replace(/[&<>"']/g, (m: string) => {
    return map[m];
  });
}

/**
 * Guard method for determing if an object (a thing) has a type property with a type of string
 *
 * @param thing {any} Any object (thing)
 * @returns {boolean} Is an object (a thing) with a type property with type of string
 */
export function isThingWithType(thing: any): thing is { type: string } {
  return typeof thing.type === 'string';
}
