/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import * as commander from 'commander';
import {readFileSync} from 'fs';
import {join} from 'path';
import {logger} from './helpers';
import {validateFiles} from './validator';

const pkgJson = JSON.parse(readFileSync(join(__dirname, '..', 'package.json')).toString());

let schemaDir: string = '';
let resDir: string = '';

commander
  .version(pkgJson.version);

commander
  .description('validate generated JSON schema files with test files')
  .arguments('<schemadir> <resdir> [reportDir]')
  .action(async (s, res, rep) => {
    schemaDir = s;
    resDir = res;
    try {
      await validateFiles(s, res, rep);
      logger.ok('Finished validation.');
    } catch (err) {
      logger.error(err);
    }
  });

commander.parse(process.argv);

[schemaDir, resDir].forEach((arg) => {
  if (arg.length === 0) {
    logger.error('Arguments are required.');
    process.exit(1);
  }
});
