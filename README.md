# @openstapps/core-validator

## What is StAppsCore?

The StAppsCore (SC) is a generalized model of data.

SC is based on the idea of [schema.org](https://www.schema.org). The SC structures data by describing objects as so called SC-types like: Event, Place, Date, Person, ...

Data sources of the same type of data are assigned to the same SC-type.

## What is StAppsCore Validator?

The StAppsCore Validator is a tool for run-time validation of objects (determining wether a JavaScript/JSON object is a valid object of the corresponding SC-type. It consumes JSON schema files from StAppsCore as the definitions of SC-types against which are validated concrete (actual) objects (as an example SCDish object in the example below).

## Use the validator

You can use the validator programatically:

```typescript
import {SCValidator} from '@openstapps/core-validator';
import {SCDish} from '@openstapps/core';
import {ValidatorResult} from 'jsonschema';
import {join} from 'path';

const objectToValidate: SCDish = {
type: 'Dish',
// more properties
};

// instantiate a new validator
const validator = new SCValidator(join('path', 'to', 'schema', 'files'));

// make the validator read the schema files
validator.feedValidator();

// validate an object
const result: ValidatorResult = validator.validate(objectToValidate);
```

## Using validateFiles function

The JSON files passed to the validateFiles method have an added layer.
That layer encapsulates the actual JSON data of the object to be verified and adds a property to enable true negative testing.

Your basic JSON object:

```json
{
  "property1": "value1",
  "property2": "value2",
  ...
}
```

JSON for validateFiles:
```json
{
  "validationErrorNames": [],
  "scData":{
    "property1": "value1",
    "property2": "value2",
    ...
    }
}
```

Where `validationErrorNames` holds the string values of the name property of the expected ValidationErrors from JSON Schema.
Empty array means no errors are expected.

## How to use StAppsCore Converter as a CLI tool (executable)?

Installation of the npm package (using `npm install`) makes the tool available as an executable with the name `openstapps-validate`.

The command `openstapps-validate` can then be called using two required arguments and one optional:

```bash
node_modules/.bin/openstapps-validate <schemadir> <resdir> [reportDir]
```
where:
- `<schemadir>` is a directory (relative path to the current directory) where JSON schema files are, e.g. `lib/schema`,
- `<resdir>` is a directory (relative path to the current directory) where test files are, e.g. `src/test/resources`,
- `[reportDir]` is a directory where HTML report of the validation will be saved to, e.g. `report` (optional argument - if it's not provided `report` will be used).

Command with the example arguments is then for example:
```bash
node_modules/.bin/openstapps-validate lib/schema src/test/resources
```

Inside of a script in `package.json` or if the npm package is installed globally, the tool `openstapps-validate` can be called without its local path (`node_modules/.bin`):

```bash
openstapps-validate <schemadir> <resdir> [reportDir]
```
